Source: rust-generic-array
Section: rust
Priority: optional
Build-Depends: debhelper (>= 12),
 dh-cargo (>= 24),
 cargo:native <!nocheck>,
 rustc:native <!nocheck>,
 libstd-rust-dev <!nocheck>,
 librust-typenum-1+default-dev (>= 1.12-~~) <!nocheck>,
 librust-version-check-0.9+default-dev <!nocheck>
Maintainer: Debian Rust Maintainers <pkg-rust-maintainers@alioth-lists.debian.net>
Uploaders:
 kpcyrd <git@rxv.cc>
Standards-Version: 4.5.0
Vcs-Git: https://salsa.debian.org/rust-team/debcargo-conf.git [src/generic-array]
Vcs-Browser: https://salsa.debian.org/rust-team/debcargo-conf/tree/master/src/generic-array
Rules-Requires-Root: no

Package: librust-generic-array-dev
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 librust-typenum-1+default-dev (>= 1.12-~~),
 librust-version-check-0.9+default-dev
Suggests:
 librust-generic-array+serde-dev (= ${binary:Version})
Provides:
 librust-generic-array+default-dev (= ${binary:Version}),
 librust-generic-array+more-lengths-dev (= ${binary:Version}),
 librust-generic-array-0-dev (= ${binary:Version}),
 librust-generic-array-0+default-dev (= ${binary:Version}),
 librust-generic-array-0+more-lengths-dev (= ${binary:Version}),
 librust-generic-array-0.14-dev (= ${binary:Version}),
 librust-generic-array-0.14+default-dev (= ${binary:Version}),
 librust-generic-array-0.14+more-lengths-dev (= ${binary:Version}),
 librust-generic-array-0.14.4-dev (= ${binary:Version}),
 librust-generic-array-0.14.4+default-dev (= ${binary:Version}),
 librust-generic-array-0.14.4+more-lengths-dev (= ${binary:Version})
Description: Generic types implementing functionality of arrays - Rust source code
 This package contains the source for the Rust generic-array crate, packaged by
 debcargo for use with cargo and dh-cargo.

Package: librust-generic-array+serde-dev
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 librust-generic-array-dev (= ${binary:Version}),
 librust-serde-1-dev
Provides:
 librust-generic-array-0+serde-dev (= ${binary:Version}),
 librust-generic-array-0.14+serde-dev (= ${binary:Version}),
 librust-generic-array-0.14.4+serde-dev (= ${binary:Version})
Description: Generic types implementing functionality of arrays - feature "serde"
 This metapackage enables feature "serde" for the Rust generic-array crate, by
 pulling in any additional dependencies needed by that feature.
